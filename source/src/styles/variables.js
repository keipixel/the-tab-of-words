module.exports = {
  font: 'Avenir, Helvetica, Arial, "Hiragino Sans", "MS Gothic", sans-serif',
  fontSerif: '"Hiragino Mincho ProN W6", "MS Mincho", serif',
  darkBlue: '#2c3e50',
  lightBlue: '#6286a9',
  red: '#cc3750'
}
