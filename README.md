# 言葉 の Tab

言葉 の Tab. A chrome extension to study Japanese word in each new tab.

![](./screenshot.png)

## Installation

- Open `chrome://extensions/` in Chrome
- Drag `the-tab-of-words.crx` to Chrome
